<!--
SPDX-FileCopyrightText: 2020 Evandro Chagas Ribeiro da Rosa <evandro@quantuloop.com>
SPDX-FileCopyrightText: 2020 Rafael de Santiago <r.santiago@ufsc.br>

SPDX-License-Identifier: Apache-2.0
-->

# Ket Bitwise Simulator

Ket Bitwise Simulator (KBW) is a noise-free quantum computer simulator that allows anyone to test quantum applications on classical computers

KBW features three simulation methods:

* **Dense** simulation, based on state vector simulation;
* **Dense v2**, which has a smaller memory footprint; and
* **Sparse** simulation, based on the Bitwise representation.

This project is part of the Ket Quantum Programming platform, see the documentation for
more information <https://quantumket.org>.

## License

Libket is released under the Apache-2.0 License. See [LICENSE](LICENSE) for more information.
